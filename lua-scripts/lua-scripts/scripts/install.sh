#!/usr/bin/env bash
install_maxlibdb(){
    echo "Installing maxlibdb .."
    cd /tmp
    wget https://github.com/maxmind/libmaxminddb/releases/download/1.3.2/libmaxminddb-1.3.2.tar.gz
    tar -vzxf libmaxminddb-1.3.2.tar.gz
    cd libmaxminddb-1.3.2
    ./configure
    make
    make check
    make install
    ldconfig
    echo "Completed installing maxlibdb!"
}

install_geoipupdate(){
    echo "Installing geoipupdate..."
    add-apt-repository ppa:maxmind/ppa
    apt update
    apt install geoipupdate
    mkdir /usr/share/GeoIP
    geoipupdate -f /usr/local/openresty/nginx/lua-scripts/geoipupdate-conf/GeoIP.conf -d /usr/share/GeoIP
    chown -R www-data:www-data /usr/share/GeoIP
    echo "Completed installing geoipupdate"
}

pull_ngx_http_geoip2_module(){
    echo "Pulling geoip2 module"
    cd /tmp
    wget https://github.com/leev/ngx_http_geoip2_module/archive/3.3.tar.gz
    tar -vzxf 3.3.tar.gz
    echo "Completed geoip2 module"
}

install_openresty(){
    echo "Installing openresty..."
    cd /tmp
    cp /usr/local/openresty/nginx /usr/local/openresty/nginx_bak
    wget https://openresty.org/download/openresty-1.13.6.2.tar.gz
    tar -vzxf openresty-1.13.6.2.tar.gz
    cd openresty-1.13.6.2
    ./configure --with-compat \
    --prefix=/usr/local/openresty \
	--with-file-aio \
	--with-http_addition_module \
	--with-http_auth_request_module \
	--with-http_dav_module \
	--with-http_flv_module \
	--with-http_geoip_module=dynamic \
	--with-http_gunzip_module \
	--with-http_gzip_static_module \
	--with-http_image_filter_module=dynamic \
	--with-http_mp4_module \
	--with-http_random_index_module \
	--with-http_realip_module \
	--with-http_secure_link_module \
	--with-http_slice_module \
	--with-http_ssl_module \
	--with-http_stub_status_module \
	--with-http_sub_module \
	--with-http_v2_module \
	--with-ipv6 \
	--with-mail \
	--with-mail_ssl_module \
	--with-md5-asm \
	--with-pcre-jit \
	--with-sha1-asm \
	--with-stream \
	--with-stream_ssl_module \
	--with-threads \
	--with-http_iconv_module \
	--with-http_realip_module \
	--with-http_stub_status_module \
	--add-module=/tmp/ngx_http_geoip2_module-3.3
    make
    make install
    chown -R www-data:www-data /usr/local/openresty
    echo "Completed installing openresty..."
}

install_opm(){
    echo "Installing dependencies by OPM..."
    /usr/local/openresty/bin/opm get jxskiss/ssl-cert-server
    /usr/local/openresty/bin/opm get openresty/lua-resty-core
    /usr/local/openresty/bin/opm get doujiang24/lua-resty-kafka
    /usr/local/openresty/bin/opm get jkeys089/lua-resty-hmac
    /usr/local/openresty/bin/opm get hamishforbes/lua-ffi-zlib
    /usr/local/openresty/bin/opm get hamishforbes/lua-resty-iputils
    echo "Completed installing dependencies by OPM"
}

install_luarocks(){
    echo "Installing dependencies by Luarocks..."
    /usr/local/openresty/luajit/bin/luarocks install luasocket
    /usr/local/openresty/luajit/bin/luarocks install lua-resty-healthcheck
    /usr/local/openresty/luajit/bin/luarocks install penlight
    /usr/local/openresty/luajit/bin/luarocks install lua-toml
    /usr/local/openresty/luajit/bin/luarocks install luaossl
    /usr/local/openresty/luajit/bin/luarocks install lapis
    /usr/local/openresty/luajit/bin/luarocks install date
    /usr/local/openresty/luajit/bin/luarocks install lsqlite3
    /usr/local/openresty/luajit/bin/luarocks install lua-resty-jit-uuid
    /usr/local/openresty/luajit/bin/luarocks install psl
    /usr/local/openresty/luajit/bin/luarocks install lua-resty-iputils
    echo "Completed installing dependencies by Luarocks"
}

install_from_http(){
    echo "Installing from http..."
    curl https://codeload.github.com/Cyan4973/xxHash/tar.gz/v0.7.0 --out /tmp/xxhash.tar.gz \
    && cd /tmp/ && tar -zxvf /tmp/xxhash.tar.gz && mv xxHash-0.7.0 xxhash \
    && cd /tmp/xxhash && gcc xxhash.c -O3 -o libxxhash.so -shared -fPIC \
    && mv libxxhash.so /usr/local/openresty/lualib/ \
    && rm -r /tmp/xxhash /tmp/xxhash.tar.gz
    echo "Completed installing from http"
}

restart_openresty(){
    echo "Restarting openresty ..."
    systemctl restart openresty.service
    echo "Completed restarting openresty ..."
}



run(){
    install_maxlibdb
    install_geoipupdate
    pull_ngx_http_geoip2_module
    # install_openresty
    # install_opm
    # install_luarocks
    # install_from_http
    restart_openresty
}
apt install wget
run
luarocks install luajit-geoip
