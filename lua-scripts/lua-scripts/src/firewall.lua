local middleware = require("lua-scripts/src/rewrite_handler/middleware")
local redis_conn = require "lua-scripts/src/redis"


local function check_firewall_rules()
    local prepare_request_data = require("lua-scripts/src/rule_engine/prepare_request_data")
    request_info = prepare_request_data.prepare_request_data()
    -- request = request_info

    local rulesJson = redis_conn.get_cache_by_key("rules")
    local cjson = require "cjson"
    cjson.decode_array_with_array_mt(true)
    local rules = cjson.decode(rulesJson)
    local check_rules = require("lua-scripts/src/rule_engine/check_rule")
    for _, v in pairs(rules) do
        -- check
        check_rules.req = request_info
        local is_match = check_rules.check_rule(v)
        if is_match then
            -- fixme: reconsider priority allow and block?
            if v.action == "allow" then
                ngx.say("Allow this request")
                return
            elseif v.action == "block" then
                ngx.say("Block this request")
            end
        end
        do return end
        ngx.say(cjson.encode(v))
    end
    
    
end

middleware.use(check_firewall_rules)