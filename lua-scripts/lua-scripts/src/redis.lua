local _M = {}

function _M.myprint(param)
    ngx.say("This is my print function -   ##",param,"##")
 end

 local redis = require "resty.redis"
 local REDIS_HOST = "172.27.0.2"
 local REDIS_PORT = 6379

 -- create connection
 function connect_redis(redis_host, redis_port)
    local red = redis:new()
    red:set_timeouts(1000, 1000, 1000)
    local ok, err = red:connect(redis_host, redis_port)
    if not ok then
        ngx.say("failed to connect to redis: ", err)
        return nil, err
    end
    return red
 end

--  -- get cache by key
 function _M.get_cache_by_key(key)
    local r, err = connect_redis(REDIS_HOST, REDIS_PORT)
    if not r then
        ngx.say(ERR, "Error when connect_redis: ", err)
        return nil, err
    end
    local res, err = r:get(key)
    if not res then
        ngx.say("failed to get rules: ", err)
        return nil, err
    end
    if res == ngx.null then
        ngx.say("rules not found.")
        return nil, "Not found"
    end
    return res
 end

 -- test get rules
--  function _M.get_rules() 
--     local redis = require "resty.redis"
--     local red, err = connect_redis(REDIS_HOST, REDIS_PORT)
--     if not red then
--         ngx.say(ERR, "Error when connect_redis: ", err)
--         return nil, err
--     end
--     local res, err = red:get("rules")
--     if not res then
--         ngx.say("failed to get rules: ", err)
--         return
--     end

--     if res == ngx.null then
--         ngx.say("rules not found.")
--         return
--     end

--     ngx.say("rules: ", res)
     
--  end
return _M 