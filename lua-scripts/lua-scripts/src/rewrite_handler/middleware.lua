local _M = {}


local function global_middleware()
    -- ip blacklist
--    ngx.say("hello")
end

function _M.use(...)
    local ngx_ctx = ngx.ctx
    if ngx_ctx.global_middleware then
        ngx_ctx.global_middleware()
    else
        ngx_ctx.global_middleware = global_middleware
        ngx_ctx.global_middleware()
    end

    for i = 1, select("#", ...) do
        local v = select(i, ...)
        if type(v) == "function" then
            v()
        end
    end
end

return _M