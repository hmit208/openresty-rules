local _M = {}
local cjson = require("cjson")
local operator_switch = require("lua-scripts/src/rule_engine/operator_switch")
local rule_utils = require("lua-scripts/src/utils/rule_utils")
local recursively_checking_func

local function is_empty(s)
    return s == nil or s == ''
  end
local NEED_BLOCK = true
_M.req = {}

-- logic switch

local handle_or = function (t)
    local result = false
    for k, v in pairs(t) do
        result = recursively_checking_func(v)
        -- ngx.say("result handle_or: ".. cjson.encode(v) .. "---", result)
        if result then
            return true
        end
    end
    return result
end

local handle_and = function (t)
    local result = true
    for k, v in pairs(t) do
        result = recursively_checking_func(v)
        if not result then
            return false
        end
    end
    return result
end

local handleSingleCriteria = function (field, t)
    local operator, operand = rule_utils.get_first_key_value(t)
    -- assign request data
    operator_switch.req = _M.req
    local result = operator_switch:case(operator, operand, field)
    return result
end
local logical_switch =  {
    ["OR"] = function (x, t)
        -- local result = true
        local result = handle_or(t)
        ngx.say("result handle_or: ", result)
        -- ngx.say(result)
        return result
    end,
    ["AND"] = function (x, t)
        local result = handle_and(t)
        return result
    end,
    default = function (x, t)
        return handleSingleCriteria(x, t)
    end,
}

function logical_switch.case(self,x, t)
    local f=self[x] or self.default
    if f then
      if type(f)=="function" then
        return f(x,t, self)
      else
        error("case "..tostring(x).." not a function")
      end
    end
    return nil
end



recursively_checking_func = function (rule)
    if get_table_size(rule) ~= 1 then
        ngx.log(ngx.ERR, "Expected single entry")
        return false
    end
    local result = false
    for k, v in pairs(rule) do
        -- check 
        logical_switch.req = _M.req
        result = logical_switch:case(k, v)
    end
    return result
end

function _M.check_rule(rule)
    ngx.say("req")
    ngx.say(cjson.encode(_M.req))
    local is_match = false
    if is_empty(rule) then
        return is_match
    end
    if is_empty(rule.rule) then
        -- if rule.action == "block" then
        --     return is_match
        -- end
        return is_match
    end
    local ruleTable = cjson.decode(rule.rule)
    is_match = recursively_checking_func(ruleTable)
    ngx.say("is_match: ", is_match)
    return is_match

    -- ngx.say((rule.rule))
end


function get_table_size(t)
    local count = 0 
    if type(t) ~= "table" then
        return -1
    end
    for _ in pairs(t) do count = count + 1 end
    return count
end
return _M