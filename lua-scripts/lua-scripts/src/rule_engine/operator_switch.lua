local rule_utils = require("lua-scripts/src/utils/rule_utils")

local _M = {
    -- request = {},
    ["eq"] = function (operator, operand, field)
        --print("equal", operator)
        --print("field", field)
        --print("operand: ", operand)
        if request_info[field] then
            print ("match: ", request_info[field] == operand)
            return request_info[field] == operand
        end
        return false
    end,
    ["ne"] = function (operator, operand, field)
        print("not equal", operator)
        if request_info[field] then
            --print ("match: ", request_info[field] == operand)
            --print("-----------", request_info[field] ~= operand)
            return request_info[field] ~= operand
        end
        return false
    end,
    ["in"] = function (operator, operand, field)
        if type(operand) ~= "table" then
            return false
        end
        local result = rule_utils.has_value(operand, request_info[field])
        --print("result in: ", result)
        --print("in", operator)
        return result
    end,
    ["not in"] = function (operator, operand, field)
        print(operator)
    end,
    ["lt"] = function (operator, operand, field)
        print(operator)
    end,
    ["gt"] = function (operator, operand, field)
        print(operator)
    end,
    ["le"] = function (operator, operand, field)
        print(operator)
    end,
    ["ge"] = function (operator, operand, field)
        print(operator)
    end,
    ["contain"] = function (operator, operand, field)
        print(operator)
    end,
    ["not contain"] = function (operator, operand, field)
        print(operator)
    end,
    default = function (x, t)
        print("invalid operator " .. x)
    end
}

function _M.case(self, x, t, field)
    local f=self[x] or self.default
    if f then
      if type(f)=="function" then
        return f(x,t, field, self)
      else
        error("case "..tostring(x).." not a function")
      end
    end
    return nil
end

return _M