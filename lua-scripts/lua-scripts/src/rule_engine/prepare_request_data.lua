local _M = {}
--local middleware = require("lua-scripts/src/rewrite_handler/middleware")

--local redis_conn = require "lua-scripts/src/redis"
local cjson = require("cjson")
local pcall = pcall
local geoip = require "geoip.mmdb"
local mmdbCountry = assert(geoip.load_database("/usr/share/GeoIP/GeoLite2-Country.mmdb"))
local mmdbASN = assert(geoip.load_database("/usr/share/GeoIP/GeoLite2-ASN.mmdb"))

function _M.prepare_request_data()
    local countryCode = ""
    local asnum = ""
    local ok, geoipInfoCountry = pcall(mmdbCountry.lookup, mmdbCountry, "222.252.43.55")
    if ok then
        countryCode = geoipInfoCountry.country.iso_code
    else
        ngx.log(ngx.DEBUG, "not found country")
    end
    local ok, geoipInfoASN = pcall(mmdbASN.lookup, mmdbASN, "222.252.43.55")
    if ok then
        asnum = geoipInfoASN.autonomous_system_number
        -- ngx.say(geoipInfoASN.autonomous_system_number)
    else
        ngx.log(ngx.DEBUG, "not found ASN")
    end
    local reqData = {}
    local keys = {
        httpHost = "http.host",
        httpMethod = "http.request.method",
        httpReqUri = "http.req.uri",
        httpReqUriPath = "http.request.uri.path",
        httpUserAgent = "http.user_agent",
        ipGeoIPASNum = "ip.geoip.asnum",
        ipGeoIPCountry = "ip.geoip.country",
        ipSrc = "ip.src"
    }
    reqData[keys["httpReqUri"]] = ngx.var.request_uri
    reqData[keys["httpHost"]] = ngx.var.host
    local tmp = {}
    for str in string.gmatch(ngx.var.request_uri, "([^?]+)") do
        table.insert(tmp, str)
    end
    reqData[keys["httpReqUriPath"]] = tmp[1] or ""
    reqData[keys["httpUserAgent"]] = ngx.req.get_headers()['User-Agent']
    reqData[keys["httpMethod"]] = ngx.req.get_method()
    reqData[keys["ipGeoIPCountry"]] = countryCode
    reqData[keys["ipGeoIPASNum"]] = asnum
    reqData[keys["ipSrc"]] = ngx.var.remote_addr

    reqData["asd"] = reqData["http.req.uri"] == "/lo/asdasdas/asdasd"
    --local json = cjson.encode(reqData)

    -- ngx.say(json)
    return reqData
end

return _M