local _M = {}

function _M.get_first_key_value(t)
    if type(t) ~= "table" then
        return nil, nil
    end
    for k, v in pairs(t) do
        return k, v
    end
end


function _M.has_value (tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end

    return false
end
return _M